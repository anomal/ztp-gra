# The Best Game Ever


Welcome to the JavaScript game with Symfony2-based backend! 
There are only few steps you need to perform in order to get that running.

## Installation

1. Clone this repository to desired location on your machine
2. Find file `app/config/parameters.yml` and replace database credentials with yours (I'm assuming the database already exists, we need to fill it with tables now)
3. Go to your Terminal (console) application and run following commands:
  1. `cd /path/to/your/repo`
  2. `composer install` (I'm assuming you have composer on your machine)
  3. `php app/console doctrine:schema:update --force`
  4. `php app/console bazinga:js-translation:dump` (JS translations)
4. Visit `localhost/ztp-gra/web` (or whatever location you've chosen) and see if the game is running
5. Documentation can be found under `localhost/ztp-gra/docs` URL