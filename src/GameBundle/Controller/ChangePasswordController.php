<?php
/**
 * Change Password Controller
 *
 * PHP version 5
 *
 * @category GameBundle
 * @package  Controller
 * @author   Aleksander Gustkiewicz <agustkiewicz@gmail.com>
 * @license  GNU GPL
 * @link     http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class ChangePasswordController
 *
 * @package GameBundle\Controller
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 * @author Aleksander Gustkiewicz
 * @copyright (c) 2016
 */
class ChangePasswordController extends BaseController
{
    /**
     * Change password action.
     *
     * @return RedirectResponse
     */
    public function changePasswordAction()
    {
        $user = $this->container->get('security.context')
            ->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException(
                'This user does not have access to this section.'
            );
        }

        $form = $this->container->get('fos_user.change_password.form');
        $formHandler = $this->container
            ->get('fos_user.change_password.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('success', 'Password changed!');

            $url = $this->container->get('router')
                ->generate('game_default_index');

            return new RedirectResponse($url);
        }

        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:ChangePassword:changePassword.html.'.$this->container
                ->getParameter('fos_user.template.engine'),
            array('form' => $form->createView())
        );
    }
}
