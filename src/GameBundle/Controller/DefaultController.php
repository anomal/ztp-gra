<?php
/**
 * Default controller class.
 *
 * @category GameBundle
 * @package Controller
 * @author Aleksander Gustkiewicz <agustkiewicz@gmail.com>
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController.
 *
 * @package GameBundle\Controller
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 * @author Aleksander Gustkiewicz (agustkiewicz@gmail.com)
 * @copyright (c) 2016
 */
class DefaultController extends Controller
{
    /**
     * Index action.
     *
     * @Route("/")
     * @Route("")
     *
     * @Template()
     */
    public function indexAction()
    {
        return;
    }
}
