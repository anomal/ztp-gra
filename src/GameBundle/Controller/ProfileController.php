<?php
/**
 * Profile Controller
 *
 * @category GameBundle
 * @package Controller
 * @author Aleksander Gustkiewicz <agustkiewicz@gmail.com>
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ProfileController
 *
 * @package GameBundle\Controller
 * @author Aleksander Gustkiewicz
 */
class ProfileController extends BaseController
{
    /**
     * Edit Action
     *
     * @return RedirectResponse
     */
    public function editAction()
    {
        $user = $this->container->get('security.context')
            ->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException(
                'This user does not have access to this section.'
            );
        }

        $form = $this->container->get('fos_user.profile.form');
        $formHandler = $this->container->get('fos_user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('success', 'Profile updated!');

            $url = $this->container->get('router')
                ->generate('game_default_index');

            return new RedirectResponse($url);
        }

        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:Profile:edit.html.'.$this->container
                ->getParameter('fos_user.template.engine'),
            array('form' => $form->createView())
        );
    }
}
