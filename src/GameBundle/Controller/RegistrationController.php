<?php
/**
 * Registration Controller
 *
 *  @category GameBundle
 * @package Controller
 * @author Aleksander Gustkiewicz <agustkiewicz@gmail.com>
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegistrationController
 *
 * @package GameBundle\Controller
 * @author Aleksander Gustkiewicz
 */
class RegistrationController extends BaseController
{
    /**
     * Override for registerAction from FOS bundle.
     *
     * @return RedirectResponse
     */
    public function registerAction()
    {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get(
            'fos_user.registration.form.handler'
        );
        $confirmationEnabled = $this->container->getParameter(
            'fos_user.registration.confirmation.enabled'
        );

        $process = $formHandler->process($confirmationEnabled);
        if ($process) {
            $user = $form->getData();

            $authUser = false;
            if ($confirmationEnabled) {
                $this->container->get('session')->set(
                    'fos_user_send_confirmation_email/email',
                    $user->getEmail()
                );
                $route = 'game_default_index';
            } else {
                $authUser = true;
                $route = 'game_default_index';
            }

            $this->setFlash(
                'success',
                'Congratulations! Your account is now set!'
            );
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            return $response;
        }

        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:Registration:register.html.'.$this->getEngine(),
            array(
                'form' => $form->createView(),
            )
        );
    }
}
