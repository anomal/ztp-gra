<?php
/**
 * Scores controller class.
 *
 * @copyright (c) 2016 Aleksander Gustkiewicz
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 */
namespace GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use GameBundle\Entity\Score;

/**
 * Class ScoreController.
 *
 *
 * @Route(service="app.score_controller")
 *
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 * @package GameBundle\Controller
 * @author Aleksander Gustkiewicz
 * @copyright (c) 2016
 */
class ScoreController extends Controller
{
    /**
     * Template engine.
     *
     * @var EngineInterface $templating
     */
    private $templating;

    /**
     * Model object.
     *
     * @var ObjectManager $model
     */
    private $model;

    /**
     * Entity manager.
     *
     * @var EntityManager $em
     */
    private $em;

    /**
     * Container for dependency incjection.
     *
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * Translator object.
     *
     * @var Translator $translator
     */
    private $translator;


    /**
     * ScoreController constructor.
     *
     * @param EngineInterface $templating
     * @param ObjectManager $model
     * @param EntityManager $em
     * @param Container $container
     * @param Translator $translator
     */
    public function __construct(
        EngineInterface $templating,
        ObjectManager $model,
        EntityManager $em,
        Container $container,
        Translator $translator
    ) {
        $this->templating = $templating;
        $this->model = $model->getRepository('GameBundle:Score');
        $this->em = $em;
        $this->container = $container;
        $this->translator = $translator;
    }

    /**
     * Index action.
     *
     * @Route("/scores")
     * @Route("/scores/")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws NotFoundHttpException
     */
    public function indexAction()
    {
        $scores = $this->model->findBestScores();

        return $this->templating->renderResponse(
            'GameBundle:Scores:index.html.twig',
            array('scores' => $scores)
        );
    }

    /**
     * Add action.
     *
     * @Route("/scores/add")
     * @Route("/scores/add/")
     * @param Request $request
     * @return mixed
     */
    public function addAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $score = new Score();

        $points = $request->request->get('points');
        $userId = $request->request->get('userId');
        $carAmount = $request->request->get('carAmount');
        $enemiesSpeed = $request->request->get('enemiesSpeed');
        $carSpeed = $request->request->get('carSpeed');
        $user = $userManager->findUserBy(array('id' => $userId));
        $date = new \DateTime("now");

        if (!is_null($points) && !is_null($userId)) {
            $score->setScore($points);
            $score->setUser($user);
            $score->setCreatedAt($date);
            $score->setCarAmount($carAmount);
            $score->setEnemiesSpeed($enemiesSpeed);
            $score->setCarSpeed($carSpeed);

            $this->em->persist($score);
            $this->em->flush();
        }

        return $this->templating->renderResponse(
            'GameBundle:Scores:add.html.twig',
            array('score' => $score)
        );
    }
}
