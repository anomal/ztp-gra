<?php
/**
 * Users controller Class
 *
 * @copyright (c) 2016 Aleksander Gustkiewicz
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 */
namespace GameBundle\Controller;

use FOS\UserBundle\Model\UserManager;
use GameBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Class UserController.
 *
 *
 * @Route(service="app.user_controller")
 *
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp_gra
 * @package GameBundle\Controller
 * @author Aleksander Gustkiewicz
 * @copyright (c) 2016
 */
class UserController extends Controller
{
    /**
     * Template engine.
     *
     * @var EngineInterface $templating
     */
    private $templating;


    /**
     * Container for dependency incjection.
     *
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * User Manager
     *
     * @var UserManager $userManager
     */
    protected $userManager;

    /**
     * FormFactory object.
     *
     * @var FormFactory $formFactory
     */
    private $formFactory;

    /**
     * Routing object.
     *
     * @var RouterInterface $router
     */
    private $router;

    /**
     * Session object.
     *
     * @var Session $session
     */
    private $session;

    /**
     * Translator object.
     *
     * @var Translator $translator
     */
    private $translator;

    /**
     * UserController constructor.
     *
     * @param EngineInterface $templating
     * @param Container $container
     * @param UserManager $userManager
     * @param FormFactory $formFactory
     * @param RouterInterface $router
     * @param Session $session
     * @param Translator $translator
     */
    public function __construct(
        EngineInterface $templating,
        Container $container,
        UserManager $userManager,
        FormFactory $formFactory,
        RouterInterface $router,
        Session $session,
        Translator $translator
    ) {
        $this->templating = $templating;
        $this->container = $container;
        $this->userManager = $userManager;
        $this->formFactory = $formFactory;
        $this->router = $router;
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * User game settings.
     *
     * @Route("/settings")
     * @Route("/settings/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function settingsAction(Request $request)
    {
        if ($this->container->get('security.context')->isGranted('ROLE_USER')) {
            $user = $this->container->get('security.context')
                ->getToken()->getUser();

            $settingsForm = $this
                ->formFactory
                ->create(
                    new UserType(),
                    $user,
                    array(
                        'action' => $this->generateUrl('game_user_settings')
                    )
                );

            $settingsForm->handleRequest($request);

            if ($settingsForm->isValid()) {
                $settings = $settingsForm->getData();

                $user->setCarSpeed($settingsForm['carSpeed']->getData());
                $user->setCarAmount($settingsForm['carAmount']->getData());
                $user->setEnemiesSpeed(
                    $settingsForm['enemiesSpeed']->getData()
                );

                $this->userManager->updateUser($user);

                $this->session->getFlashBag()->set(
                    'success',
                    $this->translator->trans('Settings saved!')
                );
                return new RedirectResponse(
                    $this->router->generate(
                        'game_default_index'
                    )
                );
            }

            return $this->templating->renderResponse(
                'GameBundle:User:settings.html.twig',
                array('form' => $settingsForm->createView())
            );
        } else {
            return $this->templating->renderResponse(
                'GameBundle:User:settings.html.twig',
                array()
            );
        }
    }
}
