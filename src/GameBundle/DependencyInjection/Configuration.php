<?php
/**
 * Configuration dependencies
 *
 * @category GameBundle
 * @package DependencyInjection
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * This is the class that validates and merges configuration
 * from your app/config files.
 *
 * @package GameBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('game');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
