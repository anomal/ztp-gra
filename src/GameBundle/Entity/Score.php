<?php
/**
 * Score Entity
 *
 * @category GameBundle
 * @package Entity
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */

namespace GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use GameBundle\Entity\User;

/**
 * Class Score.
 *
 * @package Entity
 * @author Aleksander Gustkiewicz
 *
 * @ORM\Table(name="scores")
 * @ORM\Entity(repositoryClass="GameBundle\Repository\Score")
 */
class Score
{
    /**
     * @ORM\Id
     * @ORM\Column(
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer id
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="scores")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @var User user
     */
    private $user;

    /**
     * @ORM\Column(
     *     name="score",
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @Assert\NotBlank
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}.",
     *     groups={"score-default"}
     *     )
     *
     * @var integer score
     */
    private $score;

    /**
     * Created at.
     *
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime"
     * )
     * @Assert\NotBlank
     *
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(
     *     name="car_speed",
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @Assert\NotBlank
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}.",
     *     groups={"score-default"}
     *     )
     *
     * @var integer $carSpeed
     */
    private $carSpeed;

    /**
     * @ORM\Column(
     *     name="car_amount",
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @Assert\NotBlank
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}.",
     *     groups={"score-default"}
     *     )
     *
     * @var integer $carAmount
     */
    private $carAmount;

    /**
     * @ORM\Column(
     *     name="enemies_speed",
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned" = true
     *     }
     * )
     * @Assert\NotBlank
     * @Assert\Type(
     *     type="numeric",
     *     message="The value {{ value }} is not a valid {{ type }}.",
     *     groups={"score-default"}
     *     )
     *
     * @var integer $enemiesSpeed
     */
    private $enemiesSpeed;


    /**
     * Get id
     *
     * @return integer Result
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Score
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return Score
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Score
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set carSpeed
     *
     * @param integer $carSpeed
     * @return Score
     */
    public function setCarSpeed($carSpeed)
    {
        $this->carSpeed = $carSpeed;

        return $this;
    }

    /**
     * Get carSpeed
     *
     * @return integer
     */
    public function getCarSpeed()
    {
        return $this->carSpeed;
    }

    /**
     * Set carAmount
     *
     * @param integer $carAmount
     * @return Score
     */
    public function setCarAmount($carAmount)
    {
        $this->carAmount = $carAmount;

        return $this;
    }

    /**
     * Get carAmount
     *
     * @return integer
     */
    public function getCarAmount()
    {
        return $this->carAmount;
    }

    /**
     * Set enemiesSpeed
     *
     * @param integer $enemiesSpeed
     * @return Score
     */
    public function setEnemiesSpeed($enemiesSpeed)
    {
        $this->enemiesSpeed = $enemiesSpeed;

        return $this;
    }

    /**
     * Get enemiesSpeed
     *
     * @return integer
     */
    public function getEnemiesSpeed()
    {
        return $this->enemiesSpeed;
    }
}
