<?php
/**
 * User Entity
 *
 * @category GameBundle
 * @package Entity
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User.
 *
 * @package Entity
 * @author Aleksander Gustkiewicz
 * @ORM\Entity
 * @ORM\Table(name="fos_users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Score", mappedBy="user")
     * @var array $scores
     */
    private $scores;

    /**
     * @ORM\Column(
     *     name="car_speed",
     *     type="integer",
     *     nullable=true,
     *     options={
     *         "unsigned" = true,
     *         "default" = 6
     *     }
     * )
     * @Assert\NotBlank(groups={"user-default"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} must be a number.",
     *      groups={"user-default"}
     * )
     * @var integer $carSpeed
     */
    private $carSpeed;

    /**
     * @ORM\Column(
     *     name="car_amount",
     *     type="integer",
     *     nullable=true,
     *     options={
     *         "unsigned" = true,
     *         "default" = 70
     *     }
     * )
     * @Assert\NotBlank(groups={"user-default"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} must be a number.",
     *      groups={"user-default"}
     * )
     * @var integer $carAmount
     */
    private $carAmount;

    /**
     * @ORM\Column(
     *     name="enemies_speed",
     *     type="integer",
     *     nullable=true,
     *     options={
     *         "unsigned" = true,
     *         "default" = 5
     *     }
     * )
     * @Assert\NotBlank(groups={"user-default"})
     * @Assert\Type(
     *      type="numeric",
     *      message="The value {{ value }} must be a number.",
     *      groups={"user-default"}
     * )
     * @var integer $enemiesSpeed
     */
    private $enemiesSpeed;
    

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->scores = new ArrayCollection();
        $this->carSpeed = 6;
        $this->carAmount = 70;
        $this->enemiesSpeed = 5;
    }

    /**
     * Add scores
     *
     * @param \GameBundle\Entity\Score $scores
     * @return User
     */
    public function addScore(\GameBundle\Entity\Score $scores)
    {
        $this->scores[] = $scores;

        return $this;
    }

    /**
     * Remove scores
     *
     * @param \GameBundle\Entity\Score $scores
     */
    public function removeScore(\GameBundle\Entity\Score $scores)
    {
        $this->scores->removeElement($scores);
    }

    /**
     * Get scores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScores()
    {
        return $this->scores;
    }

    /**
     * Set carSpeed
     *
     * @param integer $carSpeed
     * @return User
     */
    public function setCarSpeed($carSpeed)
    {
        $this->carSpeed = $carSpeed;

        return $this;
    }

    /**
     * Get carSpeed
     *
     * @return integer
     */
    public function getCarSpeed()
    {
        return $this->carSpeed;
    }

    /**
     * Set carAmount
     *
     * @param integer $carAmount
     * @return User
     */
    public function setCarAmount($carAmount)
    {
        $this->carAmount = $carAmount;

        return $this;
    }

    /**
     * Get carAmount
     *
     * @return integer
     */
    public function getCarAmount()
    {
        return $this->carAmount;
    }

    /**
     * Set enemiesSpeed
     *
     * @param integer $enemiesSpeed
     * @return User
     */
    public function setEnemiesSpeed($enemiesSpeed)
    {
        $this->enemiesSpeed = $enemiesSpeed;

        return $this;
    }

    /**
     * Get enemiesSpeed
     *
     * @return integer
     */
    public function getEnemiesSpeed()
    {
        return $this->enemiesSpeed;
    }
}
