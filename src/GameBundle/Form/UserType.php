<?php
/**
 * UserType Form
 *
 * @category GameBundle
 * @package Form
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserType
 *
 * @package GameBundle\Form
 * @author Aleksander Gustkiewicz
 */
class UserType extends AbstractType
{
    /**
     * Form builder.
     *
     * @param FormBuilderInterface $builder Form builder
     * @param array $options Form options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'id',
            'hidden',
            array('mapped' => false)
        );
        if (isset($options['validation_groups'])
            && count($options['validation_groups'])
        ) {
            $builder->add(
                'carSpeed',
                'choice',
                array(
                    'choices' => array(
                        3 => 'Slow',
                        6 => 'Normal',
                        10 => 'Fast'
                    ),
                    'attr' => array(
                        'onchange' => 'updateBgSpeed(this.value)'
                    ),
                    'preferred_choices' => array(
                        'muppets', 'arr'
                    )
                )
            );

            $builder->add(
                'carAmount',
                'choice',
                array(
                    'choices' => array(
                        120 => 'Less',
                        70 => 'Normal',
                        40 => 'More'
                    ),
                    'attr' => array(
                        'onchange' => 'updateEnemiesAmount(this.value)'
                    )
                )
            );

            $builder->add(
                'enemiesSpeed',
                'choice',
                array(
                    'choices' => array(
                        10 => 'Slow',
                        5 => 'Normal',
                        3 => 'Fast'
                    ),
                    'attr' => array(
                        'onchange' => 'updateEnemiesSpeed(this.value)'
                    )
                )
            );

            $builder->add(
                'save',
                'submit',
                array(
                    'label' => 'Save'
                )
            );
        }
    }

    /**
     * Sets default options for form.
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'GameBundle\Entity\User',
                'validation_groups' => 'user-default',
            )
        );
    }

    /**
     * Function getName()
     *
     * It gets the name of the form.
     * @return string
     */
    public function getName()
    {
        return 'user_form';
    }
}
