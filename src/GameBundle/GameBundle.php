<?php
/**
 * GameBundle
 *
 * @category GameBundle
 * @package GameBundle
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class GameBundle
 * @package GameBundle
 */
class GameBundle extends Bundle
{
}
