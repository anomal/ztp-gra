<?php
/**
 * Score repository
 *
 * @category GameBundle
 * @package Repository
 * @author Aleksander Gustkiewicz
 * @license GNU GPL
 * @link http://wierzba.wzks.uj.edu.pl/~12_gustkiewicz/ztp-gra
 */
namespace GameBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

use GameBundle\Entity\User;

/**
 * Class Score
 *
 * @package GameBundle\Repository
 * @author Aleksander Gustkiewicz
 */
class Score extends EntityRepository
{
    /**
     * Retrieve best results.
     *
     * @return array
     */
    public function findBestScores()
    {
        return $this->findBy(array(), array('score' => 'DESC'));
    }
}
