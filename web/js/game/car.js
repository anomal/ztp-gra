//Apple object
function car(options)
{
    this.height = 2.2;
    this.width = 1.2;
    this.x = options.x;
    this.y = options.y;

    this.game = options.game;

    var linear_damping = this.game.car_speed; // speed - lower value is higher speed

    var info = {
        'density' : 10 ,
        'linearDamping' : linear_damping ,
        'fixedRotation' : true ,
        'userData' : this ,
        'type' : b2Body.b2_dynamicBody ,
        'restitution': 0.3,
        'torque': 3
    };

    this.carType = options.whichCar;


    switch (this.carType) {
        case 1:
            this.img = img_res('car.png');
            break;
        case 2:
            this.img = img_res('car2.png');
            break;
        case 3:
            this.img = img_res('car3.png');
            break;
        default:
            this.img = img_res('car.png');
            break;
    }


    var body = create_box(this.game.box2d_world , this.x, this.y, this.width, this.height, info);
    this.body = body;
}

car.prototype.draw = function()
{
    if(this.body == null)
    {
        return false;
    }
    //draw_body(this.body, this.game.ctx);

    var c = this.game.get_offset(this.body.GetPosition());

    var scale = this.game.scale;

    var sx = c.x * scale;
    var sy = c.y * scale;

    var width = this.width * scale;
    var height = this.height * scale;



    this.game.ctx.translate(sx, sy);
    this.game.ctx.drawImage(this.img , -width / 2, -height / 2, width, height);
    this.game.ctx.translate(-sx, -sy);
}

car.prototype.tick = function()
{
    this.age++;

    this.add_velocity(new b2Vec2(0,-1));

    //destroy the car if it falls below the x axis
    if(this.body.GetPosition().y < 0)
    {
        this.game.destroy_object(this);
    }
}

car.prototype.add_velocity = function(vel)
{
    var b = this.body;
    var v = b.GetLinearVelocity();

    v.Add(vel);

    //check for max horizontal and vertical velocities and then set
    if(Math.abs(v.y) > this.max_ver_vel)
    {
        v.y = this.max_ver_vel * v.y/Math.abs(v.y);
    }

    if(Math.abs(v.x) > this.max_hor_vel)
    {
        v.x = this.max_hor_vel * v.x/Math.abs(v.x);
    }

    //set the new velocity
    b.SetLinearVelocity(v);
}

//Destroy the apple when player eats it
car.prototype.destroy = function()
{
    if(this.body == null)
    {
        return;
    }
    this.body.GetWorld().DestroyBody( this.body );
    this.body = null;
    this.dead = true;
}
