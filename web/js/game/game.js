/*
 Fruit Hunter Game
 Made using Box2d and Jquery on the Html5 Canvas element

 Author : Silver Moon
 m00n.silv3r@gmail.com

 Enjoy!!
 */

//Global game object
var global_game = null;

//start game once page has finished loaded
$(function() {
    start_game();
});

function start_game()
{
    var temp_player_id = null;
    var temp_car_speed = null;
    var temp_car_amount = null;
    var temp_bgSpeed = null;
    var exists = false;

    if(global_game) {
        temp_player_id = global_game.player_id;
        temp_car_speed = global_game.car_speed;
        temp_car_amount = global_game.car_amount;
        temp_bgSpeed = global_game.bgSpeed;
        exists = true;
    }

    var g = new game();

    //store game pointer in a global object
    global_game = g;

    if(exists) {
        global_game.player_id = temp_player_id;
        global_game.car_speed = temp_car_speed;
        global_game.car_amount = temp_car_amount;
        global_game.bgSpeed = temp_bgSpeed;
    }

    $(window).resize(function() {
        g.resize();
    });

    g.start();
    g.showMenu();
}

function game()
{
    this.fps = 60;
    this.scale = 50;

    //global array of all objects to manage
    this.game_objects = [];

    this.points = 0;
    this.to_destroy = [];
    this.time_elapsed = 0;

    this.player_on = false;

    // my variables
    this.player_id = 0;
    this.car_speed = 5; // less value means bigger speed ENEMIES
    this.car_amount = 70; // less value means more cars
    this.bgSpeed = 6; // its the overall game speed PLAYER
}

/**
 * Method showMenu
 *
 * This method is used to show menu and hide game controls
 */
game.prototype.showMenu = function()
{
    $('.overlay').addClass('shown');
    $('.controls').addClass('hidden');
    this.stop_handling();
}

/**
 * Method hideMenu
 *
 * This method is used to hide menu and show game controls
 */
game.prototype.hideMenu = function()
{
    $('.overlay').removeClass('shown');
    $('.controls').removeClass('hidden');
    this.start_handling();
}


game.prototype.countdown = function()
{
    var timer = $('#timer');
    var i = 3;
    timer.show();
    var id = setInterval(function(){
        if(i > 0) {
            timer.html(i);
            i = i - 1;
        }
    }, 900);
    setTimeout(function(){
        timer.html('');
        timer.hide();
        clearInterval(id);
    }, 3000);
}

/**
 * Function startGameplay
 *
 * This method is called when user clicks on "New Game" or "Resume" buttons
 *
 * @namespace game
 * @author Aleksander Gustkiewicz
 */
game.prototype.startGameplay = function()
{

    // start new game
    if (this.player != undefined ) {
        this.clear();
    }

    this.points = 0;

    $('.resume').addClass('hidden').attr('disabled', true);
    this.hideMenu();

    var that = this;

    this.countdown();

    setTimeout(function(){
        // player is added
        that.player = new player({x : that.screen_width/2 - 1.25, y: that.screen_height/3 , game : that});
        that.game_objects.push(that.player);

        //attach event handlers for key presses
        that.start_handling();

        //setup collision handler too
        that.setup_collision_handler();

        that.player_on = true;
    }, 3000);

}

/**
 * Method resume
 *
 * This method is used to resume paused game.
 */
game.prototype.resume = function()
{
    this.hideMenu();
    this.pause();
    return;
}


/**
 * Method pause
 *
 * This method is used to pause the game.
 */
game.prototype.pause = function()
{
    if (this.is_paused) { // play the game
        $('.resume').addClass('hidden').attr('disabled', true);
        var that = this;

        this.countdown();
        setTimeout(function(){
            that.is_paused = false;
            that.tick();
        }, 3000);
    } else { // pause the game
        $('.resume').removeClass('hidden').attr('disabled', false);
        this.is_paused = true;
        this.showMenu();
    }
}

game.prototype.resize = function()
{
    var canvas = this.canvas;

    //Set the canvas dimensions to match the window dimensions
    var w = $(window).outerWidth();
    var h = $(window).outerHeight();

    canvas.width(w);
    canvas.height(h);

    canvas.attr('width' , w * 0.75);
    canvas.attr('height' , h * 0.75);

    this.canvas_width = canvas.attr('width');
    this.canvas_height = canvas.attr('height');

    this.screen_height = 10;
    this.scale = this.canvas_height / this.screen_height;
    this.screen_width = this.canvas_width / this.scale;
}


/**
 * Method clear
 *
 * This method is used to clear the canvas from existing objects (cars and player).
 */
game.prototype.clear = function()
{
    this.game_objects = [];
    this.start();
}

/**
 * Method setup
 *
 * It sets the game canvas.
 */
game.prototype.setup = function()
{
    this.ctx = ctx = $('#canvas').get(0).getContext('2d');
    var canvas = $('#canvas');
    this.canvas = canvas;

    //resize to correct size
    this.resize();

    //dimensions in metres
    var w = this.screen_width;
    var h = this.screen_height;

    //create the box2d world
    this.create_box2d_world();


    // side walls
    this.game_objects.push(new wall({x:w/2 - 3, y:0, width: 0, height:100, game: this}));
    this.game_objects.push(new wall({x:w/2 + 3, y:0, width: 0, height:100, game: this}));
}

/**
 * Function create_box2d_world
 *
 * Setup world and gravity and assign those to global variables.
 */
game.prototype.create_box2d_world = function()
{
    //10m/s2 downwards, cartesian coordinates remember - we shall keep slightly lesser gravity
    // var gravity = new b2Vec2(0, -10);
    var gravity = new b2Vec2(0, 0);

    /*
     very important to do this, otherwise player will not move.
     basically dynamic bodies trying to slide over static bodies will go to sleep
     */
    var doSleep = false;
    var world = new b2World(gravity , doSleep);

    //save in global object
    this.box2d_world = world;
}

/**
 * Method start
 *
 * Start the game. Setup and start ticking the clock
 */
game.prototype.start = function()
{
    this.on = true;
    this.total_points = 0;
    this.bgY = 0;

    this.setup();
    this.is_paused = false;

    //Start the Game Loop - TICK TOCK TICK TOCK TICK TOCK TICK TOCK
    this.tick();
}

/**
 * Method redraw_world
 *
 * Redraws the canvas every clock tick
 */
game.prototype.redraw_world = function()
{
    //1. clear the canvas first - not doing this will cause tearing at world ends
    this.ctx.clearRect(0 , 0 , this.canvas_width , this.canvas_height);

    //dimensions in metres
    var w = this.screen_width;
    var h = this.screen_height;
    this.bgY += this.bgSpeed;

    var img = img_res('grass3.jpg');
    this.ctx.drawImage(img,  0 , this.bgY, this.canvas_width, this.canvas_height);
    this.ctx.drawImage(img,  0 , this.bgY - this.canvas_height , this.canvas_width, this.canvas_height);


    img = img_res('asphalt2.jpg');
    this.ctx.drawImage(img,  (w/2 - 3) * this.scale , this.bgY, 6 * this.scale, this.canvas_height);
    this.ctx.drawImage(img,  (w/2 - 3) * this.scale , this.bgY - this.canvas_height , 6 * this.scale, this.canvas_height);

    if (this.bgY >= this.canvas_height) {
        this.bgY = 0;
    }

    if (this.player_on) {
        this.points++;
    }

    write_text({x : 25 , y : 25 , font : 'bold 15px arial' , color : '#fff' , text : Translator.trans('Points', {}, 'messages') + ' ' + this.points , ctx : this.ctx})

    //Draw each object one by one , the tiles , the cars , the other objects lying here and there
    for(var i in this.game_objects)
    {
        this.game_objects[i].draw();
    }
}

/**
 * Method tick
 *
 * It's the game clock
 * @param cnt
 */
game.prototype.tick = function(cnt)
{
    if(!this.is_paused && this.on)
    {
        this.time_elapsed += 1;

        //create a random car on top
        if(this.time_elapsed % this.car_amount == 0) // how many cars are pushed
        {
            var lane = Math.random() >= 0.5;
            var pm = Math.random() >= 0.5;
            if(pm){
                var modificator = 1.5 + Math.random() / 2;
            } else {
                var modificator = 1.5 - Math.random() / 2;
            }

            var xc;
            if (lane) {
                xc = this.screen_width/2 - modificator;
            } else {
                xc = this.screen_width/2 + modificator;
            }

            var yc = this.screen_height;
            var whichCar = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
            this.game_objects.push(
                new car(
                    {
                        x : xc,
                        y : yc,
                        game:this,
                        whichCar
                    }
                )
            );
        }

        //tick all objects, if dead then remove
        for(var i in this.game_objects)
        {
            if(this.game_objects[i].dead == true)
            {
                delete this.game_objects[i];
                continue;
            }

            this.game_objects[i].tick();
        }

        //garbage collect dead things
        this.perform_destroy();

        //Step the box2d engine ahead
        this.box2d_world.Step(1/20 , 8 , 3);

        //important to clear forces, otherwise forces will keep applying
        this.box2d_world.ClearForces();

        //redraw the world
        this.redraw_world();

        if(!this.is_paused && this.on)
        {
            var that = this;
            //game.fps times in 1000 milliseconds or 1 second
            this.timer = setTimeout( function() { that.tick(); }  , 1000/this.fps);
        }
    }
}

/**
 * Method perform_destroy
 *
 * Destroys all the elems we queued to destroy earlier
 */
game.prototype.perform_destroy = function()
{
    for(var i in this.to_destroy)
    {
        this.to_destroy[i].destroy();
    }
}

/**
 * Method get_offset
 *
 * Gets the elem's position
 * @param vector
 * @return b2Vec2 vector
 */
game.prototype.get_offset = function(vector)
{
    return new b2Vec2(vector.x - 0, Math.abs(vector.y - this.screen_height));
}

/**
 * Method start_handling
 *
 * Starts keyboard events listener for car handling.
 */
game.prototype.start_handling = function()
{
    var that = this;

    $(document).on('keydown.game' , function(e)
    {
        that.key_down(e);
        return false;
    });

    $(document).on('keyup.game' ,function(e)
    {
        that.key_up(e);
        return false;
    });
}

/**
 * Method stop_handling
 *
 * Removes event listener on keyboard.
 */
game.prototype.stop_handling = function()
{
    $(document).off('keydown.game');
    $(document).off('keyup.game');
}

/**
 * Method key_down
 *
 * Performs actions on key press
 * @param e
 */
game.prototype.key_down = function(e)
{
    var code = e.keyCode;

    //LEFT
    if(code == 37)
    {
        this.player.do_move_left = true;
    }
    //RIGHT
    else if(code == 39)
    {
        this.player.do_move_right = true;
    }
}

/**
 * Method key_up
 *
 * Performs actions on key up
 * @param e
 */
game.prototype.key_up = function(e)
{
    var code = e.keyCode;

    //LEFT
    if(code == 37)
    {
        this.player.do_move_left = false;
    }
    //RIGHT
    else if(code == 39)
    {
        this.player.do_move_right = false;
    }
}

/**
 * Method setup_collision_handler
 *
 * Detects if two objects colided, and checks if those are player, car or wall
 */
game.prototype.setup_collision_handler = function()
{
    var that = this;

    //Override a few functions of class b2ContactListener
    b2ContactListener.prototype.BeginContact = function (contact)
    {
        //now come action time
        var a = contact.GetFixtureA().GetUserData();
        var b = contact.GetFixtureB().GetUserData();

        if(a instanceof player && b instanceof car)
        {
            //that.destroy_object(b);
            //that.points++;
        }

        else if(b instanceof player && a instanceof car)
        {
            //that.destroy_object(a);
            //that.points++;
        }
        //car hits a wall
        else if(a instanceof car && b instanceof wall)
        {
            that.destroy_object(a);
        }
    }
}

/**
 * Method destroy_object
 *
 * Schedule an object for destruction in next tick
 * @param obj
 */
game.prototype.destroy_object = function(obj)
{
    this.to_destroy.push(obj);
}
