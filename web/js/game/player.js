/**
 * player class constructor
 *
 * @param options
 */
function player(options)
{
    this.height = 2.5;
    this.width = 1.2;

    this.x = options.x;
    this.y = options.y;
    this.game = options.game;
    this.age = 0;

    this.do_move_left = false;
    this.do_move_right = false;
    this.max_hor_vel = 10;
    this.max_ver_vel = 4;
    this.can_move_up = true;

    var info = {
        'density' : 10 ,
        'fixedRotation' : true ,
        'userData' : this ,
        'type' : b2Body.b2_dynamicBody ,
        'restitution' : 0 ,
        'm_mass': 0
    };

    var body = create_box(this.game.box2d_world , this.x, this.y, this.width, this.height, info);
    this.body = body;
}

/**
 * Method tick
 *
 * It moves the player right and left
 */
player.prototype.tick = function()
{
    if(this.is_out())
    {
        //turn off the game
        this.game.on = false;
        this.game.player_on = false;

        start_game();
        this.save_score();
    }

    if(this.do_move_left)
    {
        this.add_velocity(new b2Vec2(-1.3,0));
    }

    if(this.do_move_right)
    {
        this.add_velocity(new b2Vec2(1.3,0));
    }

    this.age++;
}

/**
 * Method add_velocity
 *
 * It sets the horizontal velocity for steering
 * @param vel
 */
player.prototype.add_velocity = function(vel)
{
    var b = this.body;
    var v = b.GetLinearVelocity();

    v.Add(vel);

    //check for max horizontal and vertical velocities and then set
    if(Math.abs(v.y) > this.max_ver_vel)
    {
        v.y = this.max_ver_vel * v.y/Math.abs(v.y);
    }

    if(Math.abs(v.x) > this.max_hor_vel)
    {
        v.x = this.max_hor_vel * v.x/Math.abs(v.x);
    }

    //set the new velocity
    b.SetLinearVelocity(v);
}

/**
 * @var img
 *
 * User avatar
 */
player.img = img_res('player.png');

/**
 * Method draw
 *
 * It draws the user on the game canvas
 * @returns {boolean}
 */
player.prototype.draw = function()
{
    if(this.body == null)
    {
        return false;
    }

    var c = this.game.get_offset(this.body.GetPosition());

    var scale = this.game.scale;

    var sx = c.x * scale;
    var sy = c.y * scale;

    var width = this.width * scale;
    var height = this.height * scale;

    this.game.ctx.translate(sx, sy);
    this.game.ctx.drawImage(player.img , -width / 2, -height / 2, width, height);
    this.game.ctx.translate(-sx, -sy);
}

/**
 * Method is_out
 *
 * Checks if user is out of game area.
 * @returns {boolean}
 */
player.prototype.is_out = function()
{
    //if player has fallen below the 0 level of y axis in the box2d coordinates, then he is out
    if(this.body.GetPosition().y < 0)
    {
        return true;
    }

    return false;
}

/**
 * Method save_score
 *
 * Saves the score to the database via AJAX post
 */
player.prototype.save_score = function()
{
    var data = {};
        data["userId"] = this.game.player_id;
        data["points"] = this.game.points;
        data["carAmount"] = this.game.car_amount;
        data["enemiesSpeed"] = this.game.car_speed;
        data["carSpeed"] = this.game.bgSpeed;

    var url = window.location.href;

    if(url.slice(-1) == '/') {
        url = url + 'scores/add';
    } else {
        url = url + '/scores/add'
    }

    $.ajax({
        type: 'POST',
        url: url,
        data: data
    });
}