/**
 * Function renderAjaxPage
 *
 * It's used to call backend and render the returned contents in the proper area
 * @param url
 */
function renderAjaxPage(url){
    showLoading();
    hideNav();
    $.ajax({
        url: url,
        method: "GET",
        success: function(response) {
            hideLoading();
            $('#contents').html(response);
        },
        error: function() {
            $('#contents').html('<p class="error">' + Translator.trans('Sorry! An Error occured!', {}, 'messages') + '</p>');
            hideLoading();
            showNav();
        }
    });
}

/**
 * Function clickAjaxLink
 *
 * We can add .ajax class to the link, and the page it leads to will be called via $.ajax
 */
function clickAjaxLink(){
    $('.ajax').click(function(e){
        e.preventDefault();
        var link = $(this);
        renderAjaxPage(link.attr('href'));
    });
}

/**
 * Function showLoading
 *
 * It shows the spinner while waiting for some resources to load.
 */
function showLoading() {
    $('#loader').addClass('shown');
}

/**
 * Function hideLoading
 *
 * It hides the spinner when the resources are loaded.
 */
function hideLoading() {
    $('#loader').removeClass('shown');
}

/**
 * Function hideNav
 *
 * It hides main navigation of the side when some content is loaded.
 */
function hideNav() {
    $('#mainMenu').addClass('hidden');
}

/**
 * Function showNav
 *
 * It shows the navigation back.
 */
function showNav() {
    $('#mainMenu').removeClass('hidden');
}

/**
 * Function clearContents
 *
 * It is used to clear the contents area.
 */
function clearContents() {
    $('#contents').html('');
}

/**
 * Function back
 *
 * It is used to return user to main menu.
 */
function back() {
    clearContents();
    showNav();
}

/**
 * Function updateBgSpeed
 *
 * It changes the player car speed while modifying settings
 * @param speed
 */
function updateBgSpeed(speed) {
    if (speed) {
        global_game.bgSpeed = parseInt(speed);
    }
}

/**
 * Function updateEnemiesAmount
 *
 * Changes the number of cars that are pushed against the player
 * @param amount
 */
function updateEnemiesAmount(amount) {
    if (amount) {
        global_game.car_amount = parseInt(amount);
    }
}

/**
 * Function updateEnemiesSpeed
 *
 * Changes the speed enemies are moving with
 * @param speed
 */
function updateEnemiesSpeed(speed) {
    if (speed) {
        global_game.car_speed = parseInt(speed);
    }
}

/**
 * Function configureSettingsForm
 *
 * It sets selected options when the user is logged of, basing on current game settings.
 */
function configureSettingsForm() {
    var form = $('#settings-form');
    form.find('select').each(function(){
        var elem = this;
        var options = $(elem).find('option');

        if (elem.id == 'car_speed') {
            options.each(function(){
                if (this.value == global_game.bgSpeed) {
                    $(this).attr('selected', true);
                }
            });
        }
        if (elem.id == 'car_amount') {
            options.each(function(){
                if (this.value == global_game.car_amount) {
                    $(this).attr('selected', true);
                }
            });
        }
        if (elem.id == 'enemies_speed') {
            options.each(function(){
                if (this.value == global_game.car_speed) {
                    $(this).attr('selected', true);
                }
            });
        }
    });
}

function closeMessage(elem) {
    $(elem).parent().remove();
}

clickAjaxLink();